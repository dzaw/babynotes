//
//  ApiController.swift
//  babyNotes
//
//  Created by dagmara on 22/10/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ApiController: NSObject {
    
    var user: [NSManagedObject] = []
    
    func signInWithCompletion(email: String, pass: String, _ completion: @escaping (Bool) -> ()) {
        let urlPath = "https://babynotes.dagmarazawada.pl/endpoints/user-login.php"
        guard let url = URL(string: urlPath) else { return }
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        //let postString = "email=mail@mail.com&pass=password"
        let postString = "email=\(email)&pass=\(pass)"
        //print(postString)
        
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print(responseString)
            
            DispatchQueue.main.async(execute: {
            do {
                let resp = try JSONDecoder().decode(LoginResponse.self, from: responseString!.data(using: .utf8)! )
                
                if ((resp.id?.isEmpty)!) {
                    print("Error")
                    ErrorReporting.showMessage(title: "Error", msg: "login error - check credentials or contact support")
                }
                else {
                    self.saveUser(email: resp.email!, id: resp.id!, password: resp.password!)
                    ErrorReporting.showMessage(title: "Success", msg: "User \(email) logged in")
                    completion(true)
                }
            }
            catch {
                print("Error: \(error)")
                ErrorReporting.showMessage(title: "Error", msg: "Login error - check credentials or contact support. \(String(describing: responseString!))")
                completion(false)
            }
            })
            
        }
        task.resume()
    }
    
    func saveUser(email: String, id: String, password: String) {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
            let user = NSManagedObject(entity: entity, insertInto: managedContext)
            
            let date = Date()
            user.setValue(date.format(), forKey: "date")
            user.setValue(email, forKey: "email")
            user.setValue(id, forKey: "id")
            user.setValue(password, forKey: "password")
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func isUserLoggedIn() -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        do {
            user = try managedContext.fetch(fetchRequest)
            if ( user.count > 0 ){
                //print( user.first!.value(forKeyPath: "id") as! String )
                return true
            }
            else {
                return false
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return false
    }
    
    func fetchSavedUserID() -> String {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return "" }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
        var userID = ""
        do {
            user = try managedContext.fetch(fetchRequest)
            if ( user.count > 0 ){
                userID = user.first!.value(forKeyPath: "id") as! String
            }
            return userID
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return userID
    }
    
    func clearSavedUser() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
}

