//
//  RegisterViewController.swift
//  babyNotes
//
//  Created by dagmara on 24/10/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class RegisterViewController: UIViewController, WKNavigationDelegate, UIWebViewDelegate {
    
    //TODO register
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        
        // 1 content
        let url = URL(string: "https://babynotes.dagmarazawada.pl/users/join-ios.php")!
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
        webView.scrollView.bounces = false;
        webView.scrollView.isScrollEnabled = true;
        webView.scrollView.maximumZoomScale = 1.0
        webView.scrollView.minimumZoomScale = 1.0
        
        // 2 navbars
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
}
