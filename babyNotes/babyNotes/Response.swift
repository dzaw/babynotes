//
//  Response.swift
//  babyNotes
//
//  Created by dagmara on 23/10/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation

struct LoginResponse:Codable {
    let id:String?
    let email:String?
    let password:String?
}
