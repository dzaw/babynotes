//
//  ViewController.swift
//  babyNotes
//
//  Created by dagmara on 14/08/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit
import WebKit
//import SafariServices

class ViewController: UIViewController, UIWebViewDelegate {
    
    //var webView: WKWebView!
    let loadingGif = UIImage.gifImageWithName("loading")
    lazy var gifImageView = UIImageView(image: loadingGif)
    
    var apiController: ApiController = ApiController()

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loginBtn: UIBarButtonItem!
    
    @IBOutlet weak var addEventBtn: UIButton!
    @IBAction func addEventBtnTapped(_ sender: Any) {
        print("Add event")
        
        webView.stringByEvaluatingJavaScript(from: "calApp.openModal();")
        //calApp.openModal()
        
    }
    
    
    @IBAction func infoBtnTapped(_ sender: Any) {
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "loginSegue" {
            let userLoggedIn = apiController.isUserLoggedIn()
            if userLoggedIn {
                print("user logged in")
                apiController.clearSavedUser()
                ErrorReporting.showMessage(title: "Logout", msg: "User logged out")
                webView.stringByEvaluatingJavaScript(from: "$('#userID').html(0);")
                webView.stringByEvaluatingJavaScript(from: "$('#calendar').fullCalendar( 'refetchEvents' );")
                loginBtn.title = "Login"
                return false
            }
            else {
                print("user not logged in")
                loginBtn.title = "Logout"
                return true
            }
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addEventBtn.layer.cornerRadius = 40
        addEventBtn.addShadow()
        addEventBtn.contentEdgeInsets = UIEdgeInsets(top:0, left:0, bottom:10, right:0)
        
        webView.delegate = self
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo")
        imageView.image = image
        //imageView.tintColor =  self.view.tintColor
        navigationItem.titleView = imageView
        
        //TODO
        //check user logged in
        // if logged in: pass user id and load webview
        // else move to login view
        
        // 1 content
        let url = URL(string: "https://babynotes.dagmarazawada.pl/index-ios.php")!
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
        webView.scrollView.bounces = false;
        webView.scrollView.isScrollEnabled = false;
        webView.scrollView.maximumZoomScale = 1.0
        webView.scrollView.minimumZoomScale = 1.0
        
        
        
        // 2 navbars
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = true
        //navigationController?.isNavigationBarHidden = true
        //navigationController?.title = "notatnik"
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        print("Webview started Loading")
        
        //loading
        
        gifImageView.frame = CGRect(x: (self.view.frame.size.width/2 - 200), y: (self.view.frame.size.height/2 - 150), width: 400, height: 300)
        view.addSubview(gifImageView)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("Webview did finish load")
        
        //loading done
        gifImageView.removeFromSuperview()
        
        let userID = apiController.fetchSavedUserID()
        print("USER ID \(userID)")
        
        if !userID.isEmpty {
            loginBtn.title = "Logout"
        }  else {
            loginBtn.title = "Login"
        }
        
        webView.stringByEvaluatingJavaScript(from: "$('#userID').html('\(userID)');")
        
        // update user id for rest of webview fn
        if let returnedString = webView.stringByEvaluatingJavaScript(from: "$('#userID').html('\(userID)');") {
            webView.stringByEvaluatingJavaScript(from: "$('#calendar').fullCalendar( 'refetchEvents' );")
            
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        //print("view did appear")
        
        let userID = apiController.fetchSavedUserID()
        print("USER ID \(userID)")
        
        if !userID.isEmpty {
            loginBtn.title = "Logout"
        }  else {
            loginBtn.title = "Login"
        }
        
        webView.stringByEvaluatingJavaScript(from: "$('#calendar').fullCalendar( 'refetchEvents' );")
        
        let userLoggedIn = apiController.isUserLoggedIn()
        if userLoggedIn {
            print("A user logged in")
            loginBtn.title = "Logout"
            webView.stringByEvaluatingJavaScript(from: "$('#calendar').fullCalendar( 'refetchEvents' );")
        }
        else {
            print("A user not logged in")
            loginBtn.title = "Login"
        }
        
        
        
        webView.stringByEvaluatingJavaScript(from: "$('#userID').html('\(userID)');")
        
        // update user id for rest of webview fn
        if let returnedString = webView.stringByEvaluatingJavaScript(from: "$('#userID').html('\(userID)');") {
            //webView.stringByEvaluatingJavaScript(from: "alert('userid is ' + $('#userID').html() )")
            webView.stringByEvaluatingJavaScript(from: "$('#calendar').fullCalendar( 'refetchEvents' );")
            //print("the result is \(returnedString)")
        }
    }


}

