//
//  LoginViewController.swift
//  babyNotes
//
//  Created by dagmara on 12/10/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class LoginViewController: UIViewController, WKNavigationDelegate {
    
    //var webView = ViewController.webView
    
    var apiController: ApiController = ApiController()
    
    @IBOutlet weak var userField: CustomTextField!
    @IBOutlet weak var passField: CustomTextField!
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        //print("login pressed")
        apiController.signInWithCompletion(email: userField.text!, pass: passField.text!) { (result) in
            if result {
                print("user logged in")
                self.apiController.isUserLoggedIn()
            } else {
                print("user not logged in")
            }
        }        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
}
